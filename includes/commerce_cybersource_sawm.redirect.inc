<?php

/**
 * @file
 * Redirection stuff.
 */

function commerce_cybersource_sawm_redirect() {

  // 1. Get args. The path to this callback is built in the format
  // [path-to-this-callback]/[path-to-redirect-to] - e.g.
  // "cybersource_sawm/redirect/fp/clear.png" - so the redirect path is passed
  // in to this function as args.
  $args = func_get_args();

  // 2. Fetch other $_GET arguments.
  $options = array(
    'query' => drupal_get_query_parameters(),
  );

  // 3. Redirect to https://h.online-metrix.net/ . [path] ? $_GET
  $destination = 'https://h.online-metrix.net/' . implode('/', $args);

  return drupal_goto($destination, $options);

}
